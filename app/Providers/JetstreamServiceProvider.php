<?php

namespace App\Providers;

use App\Actions\Jetstream\DeleteUser;
use App\Models\User;
use ErrorException;
use Exception;
use GuzzleHttp\Psr7\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Contracts\LogoutResponse;
use Laravel\Fortify\Fortify;
use Laravel\Jetstream\Jetstream;

class JetstreamServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   */
  public function register(): void
  {
    $this->app->instance(LogoutResponse::class, new class implements LogoutResponse
    {
      public function toResponse($request)
      {
        // dd('ok');
        return redirect('/');
      }
    });
  }

  /**
   * Bootstrap any application services.
   */
  public function boot(): void
  {
    Fortify::authenticateUsing(function (Request $request) {
      $username = $request->email;

      if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
        $user = User::where('email', $request->email)->first();
      } else {
        $user = User::where('username', $request->email)->first();
      }
      // dd($user);
      if (
        $user &&
        Hash::check($request->password, $user->password)
      ) {
        return $user;
      }
    });

    $this->configurePermissions();

    Jetstream::deleteUsersUsing(DeleteUser::class);
  }

  /**
   * Configure the permissions that are available within the application.
   */
  protected function configurePermissions(): void
  {
    Jetstream::defaultApiTokenPermissions(['read']);

    Jetstream::permissions([
      'create',
      'read',
      'update',
      'delete',
    ]);
  }
}
