<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class SeederUser extends Seeder
{
  /**
   * Run the database seeds.
   */
  public function run()
  { // create user super admin
    $user = User::create([
      'name'          => 'Farhan Riuzaki',
      'username'      => 'superadmin',
      'email'         => 'admin@email.com',
      'password'      => bcrypt('P@ssw0rd')
    ]);

    // sync user Farhan Riuzaki to role super admin
    $user->syncRoles(['Super Admin']);
  }
}
