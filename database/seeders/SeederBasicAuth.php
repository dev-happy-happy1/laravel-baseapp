<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class SeederBasicAuth extends Seeder
{
  /**
   * Run the database seeds.
   */
  public function run(): void
  {
    $permissions = [
      'role.index',
      'role.create',
      'role.edit',
      'role.delete',
      'user.index',
      'user.create',
      'user.edit',
      'user.delete',
      'permission.index',
      'permission.create',
      'permission.edit',
      'permission.delete',
      'audit.index',
      'audit.create',
      'audit.edit',
      'audit.delete',
    ];

    foreach ($permissions as $permission) {
      Permission::create(['name' => $permission]);
    }

    // default role
    $role = Role::create([
      'name'  => 'Super Admin'
    ]);
    $role = Role::where('name', 'Super Admin')->first();

    // sync permissions to role
    $role->syncPermissions($permissions);
  }
}
